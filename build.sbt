name := "Arena"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "se.radley" %% "play-plugins-salat" % "1.4.0",
  "jp.t2v" %% "play2-auth"      % "0.11.0",
  "jp.t2v" %% "play2-auth-test" % "0.11.0" % "test",
  cache
)

play.Project.playScalaSettings

routesImport += "se.radley.plugin.salat.Binders._"

templatesImport += "org.bson.types.ObjectId"