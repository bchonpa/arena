package services

import com.mongodb.casbah.Imports._
import com.novus.salat._
import com.novus.salat.dao._
import play.api.Play.current
import se.radley.plugin.salat._
import models.customContext._
import com.mongodb.casbah.commons.MongoDBObject
import models.AbstractLog
import models.FollowUserLog
import models.GoodCommentLog
import models.GoodProblemLog

object LogDAO extends ModelCompanion[AbstractLog, ObjectId] {
  def collection = mongoCollection("logs")
  val dao = new SalatDAO[AbstractLog, ObjectId](collection) {}
  
  def findGoodProblemLog: List[AbstractLog] = 
    findAll().sort(orderBy = MongoDBObject("_id" -> -1)).filter {
      case _: GoodProblemLog => true
      case _ => false
    }.toList
    
  def findGoodCommentLog: List[AbstractLog] =
    findAll().sort(orderBy = MongoDBObject("_id" -> -1)).filter {
      case _: GoodCommentLog => true
      case _ => false
    }.toList
    
  def findFollowUserLog: List[AbstractLog] =
    findAll().sort(orderBy = MongoDBObject("_id" -> -1)).filter {
      case _: FollowUserLog => true
      case _ => false
    }.toList
}
