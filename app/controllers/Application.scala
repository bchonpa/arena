package controllers

import play.api._
import play.api.mvc._
import services.LogDAO
import models.GoodProblemLog
import org.joda.time.DateTime

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

}