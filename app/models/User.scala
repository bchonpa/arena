package models

import com.mongodb.casbah.Imports._
import org.joda.time.DateTime

case class User (
  id: ObjectId = new ObjectId, 
  password: String, 
  lastname: String,
  firstname: String,
  nickname: String,
  email: String,
  isAdmin: Option[Boolean],
  comment: String,
  studentId: String,
  createDate: DateTime = new DateTime()
){

}