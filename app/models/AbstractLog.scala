package models

import com.mongodb.casbah.Imports._
import org.joda.time.DateTime

abstract class AbstractLog (id: ObjectId, userId: String, createDate: DateTime)