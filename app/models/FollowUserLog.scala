package models

import com.mongodb.casbah.Imports._
import models.customContext._
import org.joda.time.DateTime

case class FollowUserLog 
(id: ObjectId = new ObjectId, userId: String, createDate: DateTime = new DateTime(), followedUserId: String) 
extends AbstractLog(id, userId, createDate)