package models

import com.novus.salat._
import com.novus.salat.dao._
import com.mongodb.casbah.Imports._
import play.api.Play
import play.api.Play.current
import com.mongodb.casbah.commons.conversions.scala.{RegisterConversionHelpers, RegisterJodaTimeConversionHelpers}

package object customContext {
  RegisterConversionHelpers()
  RegisterJodaTimeConversionHelpers() 
  
  implicit val context = {
    val context = new Context {
      val name = "custom"
      override val typeHintStrategy = StringTypeHintStrategy(when = TypeHintFrequency.Always,
        typeHint = "_typeHint")
    }
    context.registerGlobalKeyOverride(remapThis = "id", toThisInstead = "_id")
    context.registerClassLoader(cl = Play.classloader)
    context
  }
}
