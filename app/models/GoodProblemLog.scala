package models

import com.mongodb.casbah.Imports._
import models.customContext._
import org.joda.time.DateTime

case class GoodProblemLog
(id: ObjectId = new ObjectId, userId: String, createDate: DateTime = new DateTime(), problemId: String) 
extends AbstractLog(id, userId, createDate)