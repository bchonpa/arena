package models

import com.mongodb.casbah.Imports._
import models.customContext._
import org.joda.time.DateTime

case class GoodCommentLog
(id: ObjectId = new ObjectId, userId: String, createDate: DateTime = new DateTime(), commentId: String) 
extends AbstractLog(id, userId, createDate)